from fastapi import FastAPI

from .items import router as items_router

api_v2 = FastAPI()

api_v2.include_router(items_router, prefix="/items", tags=["Items"])