from typing import Optional
from fastapi import APIRouter

router = APIRouter()

@router.get("/")
def list_items():
    return [
        {
            "id": 1,
            "name": "xample 1"
        },
        {
            "id": 2,
            "name": "xample 2"
        }
    ]

@router.get("/{item_id}")
def read_items(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}