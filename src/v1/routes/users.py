from typing import Optional
from fastapi import APIRouter
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter


router = InferringRouter()


@cbv(router)
class UsersRouter:

    @router.get("/")
    def list(self) -> dict:
        return {
            "results": [
                {
                    "id": 1,
                    "name": "Nicol"
                },
                {
                    "id": 2,
                    "name": "Oriol"
                }
            ],
            "count": 2
        }

    @router.get("/{item_id}")
    def get(self, item_id: int, q: Optional[str] = None) -> dict:
        return {
            "data": {
                "id": 1,
                "name": "Nicol"
            },
            "q": q
        }