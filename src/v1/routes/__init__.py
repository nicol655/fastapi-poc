from fastapi import FastAPI

from .items import router as items_router
from .users import router as users_router

api_v1 = FastAPI()

api_v1.include_router(items_router, prefix="/items", tags=["Items"])
api_v1.include_router(users_router, prefix="/users", tags=["Users"])