from typing import Optional
from fastapi import APIRouter
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter


router = InferringRouter()


@cbv(router)
class ItemsRouter:

    @router.get("/")
    def list(self) -> dict:
        return {
            "results": [
                {
                    "id": 1,
                    "name": "xample 1"
                },
                {
                    "id": 2,
                    "name": "xample 2"
                }
            ],
            "count": 2
        }

    @router.get("/{item_id}")
    def get(self, item_id: int, q: Optional[str] = None) -> dict:
        return {
            "data": {
                "id": 1,
                "name": "xample 1"
            },
            "q": q
        }