from fastapi import FastAPI

from v1.routes import api_v1
from v2.routes import api_v2

app = FastAPI()

app.mount("/v1", api_v1)
app.mount("/v2", api_v2)